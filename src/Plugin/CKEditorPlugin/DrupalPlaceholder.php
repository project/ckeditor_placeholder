<?php

namespace Drupal\ckeditor_placeholder\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\ckeditor\CKEditorPluginConfigurableInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "drupalplaceholder" plugin.
 *
 * @CKEditorPlugin(
 *   id = "drupalplaceholder",
 *   label = @Translation("Placeholder"),
 *   module = "ckeditor_placeholder"
 * )
 */
class DrupalPlaceholder extends CKEditorPluginBase implements CKEditorPluginConfigurableInterface {

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return drupal_get_path('module', 'ckeditor_placeholder') . '/js/plugins/drupalplaceholder/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return [
      'core/drupal.ajax',
      'ckeditor_placeholder/ckeditor_placeholder.editor',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state, Editor $editor) {
    // Defaults.
    $config = [
      'style' => '[[ ]]',
      'tokens' => 'demo-token|This is a demo placeholder description.',
      'preselected' => 'free',
    ];
    $settings = $editor->getSettings();
    if (isset($settings['plugins']['drupalplaceholder'])) {
      $config = $settings['plugins']['drupalplaceholder'];
    }

    $form['style'] = [
      '#title' => $this->t('Token Style'),
      '#type' => 'select',
      '#options' => [
        '[[ ]]' => '[[token]]',
        '{{ }}' => '{{ token }}',
      ],
      '#default_value' => $config['style'],
      '#description' => $this->t('Select the style of the token'),
    ];

    $form['preselected'] = [
      '#title' => $this->t('Tokens list preselected or free style'),
      '#description' => $this->t('You can set a list of available tokens or let editor to type in any token they like'),
      '#type' => 'radios',
      '#options' => [
        'free' => $this->t('Free style'),
        'preselected' => $this->t('Preselected'),
      ],
      '#default_value' => $config['preselected'],
    ];

    // @TODO Add validation for tokens.
    $form['tokens'] = [
      '#title' => $this->t('Tokens'),
      '#type' => 'textarea',
      '#default_value' => $config['tokens'],
      '#description' => $this->t('One token per line in `<token>|<description>` format.'),
      '#states' => [
        'visible' => [
          ':input[name="editor[settings][plugins][drupalplaceholder][preselected]"]' => ['value' => 'preselected'],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    $settings = $editor->getSettings();
    $config = [
      'drupalPlaceholder_dialogTitleAdd' => $this->t('Insert Placeholder'),
      'drupalPlaceholder_dialogTitleEdit' => $this->t('Edit Placeholder'),
    ];
    if (isset($settings['plugins']['drupalplaceholder']['style'])) {
      if ($settings['plugins']['drupalplaceholder']['style'] == '[[ ]]') {
        $config['drupalPlaceholder_left'] = '[[';
        $config['drupalPlaceholder_right'] = ']]';
      }
      elseif ($settings['plugins']['drupalplaceholder']['style'] == '{{ }}') {
        $config['drupalPlaceholder_left'] = '{{ ';
        $config['drupalPlaceholder_right'] = ' }}';
      }
    }
    if (isset($settings['plugins']['drupalplaceholder']['preselected'])) {
      $config['drupalPlaceholder_preselected'] = $this->getTokens($settings['plugins']['drupalplaceholder']['preselected']);
    }
    if (isset($settings['plugins']['drupalplaceholder']['tokens'])) {
      $config['drupalPlaceholder_tokens'] = $this->getTokens($settings['plugins']['drupalplaceholder']['tokens']);
    }
    return $config;
  }

  /**
   * Generate an array of tokens from settings string.
   */
  protected function getTokens($tokens_setting) {
    $token_array = explode("\n", $tokens_setting);
    $token_array = array_filter($token_array);
    return $token_array;
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      'DrupalPlaceholder' => [
        'label' => $this->t('Placeholder'),
        'image' => drupal_get_path('module', 'ckeditor_placeholder') . '/js/plugins/drupalplaceholder/icons/drupalplaceholder.png',
      ],
    ];
  }

}
