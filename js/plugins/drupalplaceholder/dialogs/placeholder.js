
/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license
 */

/**
 * @fileOverview Definition for placeholder plugin dialog.
 *
 */

'use strict';

CKEDITOR.dialog.add( 'drupalplaceholder', function( editor ) {
	var generalLabel = Drupal.t('General'),
			validNameRegex = /^[^\[\]<>]+$/,
			validNameDescription = Drupal.t('The placeholder can not be empty and can not contain any of following characters: [, ], <, >');

	if (editor.config.drupalPlaceholder_left === '{{') {
    validNameRegex = /^[^\{\}<>]+$/;
    validNameDescription = Drupal.t('The placeholder can not be empty and can not contain any of following characters: {, }, <, >');
  }

  var formElements = [
      {
        id: 'name',
        type: 'text',
        style: 'width: 100%;',
        label: Drupal.t('Placeholder Name'),
        'default': '',
        required: true,
        validate: CKEDITOR.dialog.validate.regex( validNameRegex, validNameDescription ),
        setup: function( widget ) {
          this.setValue( widget.data.name );
        },
        commit: function( widget ) {
          widget.setData( 'name', this.getValue() );
        }
      }
  ];

  if (editor.config.drupalPlaceholder_preselected[0] === 'preselected') {
    var selectItems = [],
        description = [];
    for (var key in editor.config.drupalPlaceholder_tokens) {
      var line = editor.config.drupalPlaceholder_tokens[key];
      var lineSplit = line.split('|');
      selectItems.push([lineSplit[0]]);
      description.push('<div class="token-description hidden" data-token="' + lineSplit[0] + '">' + lineSplit[1] + '</div>');
    }
    formElements = [
      {
        type: 'select',
        id: 'sport',
        label: 'Select a token from the list below',
        items: selectItems,
        'default': '',
        setup: function( widget ) {
          this.setValue( widget.data.name );
        },
        commit: function( widget ) {
          widget.setData( 'name', this.getValue() );
        },
        onChange: function(){
          jQuery('.token-description').addClass('hidden');
          jQuery('.token-description[data-token="' + this.getValue() + '"]').removeClass('hidden');
        }
      },
      {
        type: 'html',
        html: description.join('')
      }
    ];
	}

	return {
		title: Drupal.t('Placeholder Properties'),
		minWidth: 300,
		minHeight: 80,
		contents: [
			{
				id: 'info',
				label: generalLabel,
				title: generalLabel,
				elements: formElements
			}
		]
	};
} );
